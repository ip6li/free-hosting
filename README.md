# Tests

This is a test solution for free web hosting solutions.
Only static content, this should always work.

# Usage

## New Page

```
./hugo new post/<name>.md
vi post/<name>.md
```

## Build

```
./hugo -D
```

