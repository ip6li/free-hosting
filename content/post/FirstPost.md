---
title: "FirstPost"
date: 2022-01-17T16:16:40+01:00
draft: false
---

This is the new blog of spamwc.de.

# What is spamwc.de?

Many site are collecting e-mail address to spam them. Best solution is to use e-mail addresses for one time use,
so register, get activation mail and forget mail address.

# What make spamwc.de different?

Many site are using blacklists for throe away mail adresses, so you cannot register which such addresses.
We change our mail addresse in quite short intervals, so blacklist has no chances to blacklist ours.

